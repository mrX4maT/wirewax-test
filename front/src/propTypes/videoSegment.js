import PropTypes from 'prop-types';

export const videoSegmentType = PropTypes.shape({
  _id: PropTypes.string.isRequired,
  in_frame: PropTypes.number.isRequired,
  out_frame: PropTypes.number.isRequired,
  labels: PropTypes.arrayOf([
    PropTypes.string.isRequired
  ]),
  content: PropTypes.shape().isRequired,
});
