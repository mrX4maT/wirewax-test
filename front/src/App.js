import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';

import Home from './page/home';
import Error from './page/error';

import Loader from './component/loader';

import './App.css';

class App extends Component {
  state = {};

  shouldComponentUpdate() {
    return false;
  }

  render() {
    return (
      <div className="main-wrap">
        <Switch>
          <Route
            component={Home}
            path="/"
            exact
          />

          <Route component={Error} />
        </Switch>

        <Loader />
      </div>
    );
  }
}

export default App;