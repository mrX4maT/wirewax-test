import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Spinner } from 'react-bootstrap';

import './loader.scss';

const Loader = ({ loaderView }) => {
  if (!loaderView) {
    return null;
  }

  return (
    <div className="loader">
      <div className="pageBackground" />
      <Spinner animation="border" variant="primary" />
    </div>
  );
};

Loader.propTypes = {
  loaderView: PropTypes.bool,
};

Loader.defaultProps = {
  loaderView: false,
};

const mapStateToProps = (state) => ({
  loaderView: state.loader.view,
});

export default connect(mapStateToProps)(Loader);
