import React from 'react';
import PropTypes from 'prop-types';
import { Badge } from 'react-bootstrap';
import { v4 as uuid } from 'uuid';

import { videoSegmentType } from '../../propTypes/videoSegment';

const itemListView = item => (
  <Badge key={uuid()} variant="light">{item}</Badge>
)

const SegmentItem = ({
  segment, index,
}) => (
  <tr>
    <td>{index}</td>
    <td>{segment.in_frame}</td>
    <td>{segment.out_frame}</td>
    <td>{segment.content.value.map(itemListView)}</td>
    <td>{segment.content.labels.map(itemListView)}</td>
    <td>{segment.content.location.map(itemListView)}</td>
  </tr>
);

SegmentItem.propTypes = {
  segment: videoSegmentType.isRequired,
  index: PropTypes.number.isRequired,
};

export default SegmentItem;