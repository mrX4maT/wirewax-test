import React from 'react';
import PropTypes from 'prop-types';
import { Table } from 'react-bootstrap';
import { withTranslation } from 'react-i18next';
import config from 'react-global-configuration';
import { ArrowUp, ArrowDown } from 'react-bootstrap-icons';

import SegmentItem from './item';

const defaultPageSize = config.get('videoSegmentQuerySettings.pageSize');

const VideoSegmentTable = ({ videoSegmentList, pageNumber, t, onSort, sortData }) => (
  <Table responsive className="vide-segment-table">
    <thead>
      <tr>
        <th width="5%"> </th>
        <th width="10%" onClick={() => onSort('in_frame')} className="pointer">
          {t('in_frame')}
          {sortData.sortField === 'in_frame' && sortData.sortDirection === 1 && (<ArrowDown color="royalblue" size={18} />)}
          {sortData.sortField === 'in_frame' && sortData.sortDirection === -1 && (<ArrowUp color="royalblue" size={18} />)}
        </th>
        <th width="12%" onClick={() => onSort('out_frame')} className="pointer">
          {t('out_frame')}
          {sortData.sortField === 'out_frame' && sortData.sortDirection === 1 && (<ArrowDown color="royalblue" size={18} />)}
          {sortData.sortField === 'out_frame' && sortData.sortDirection === -1 && (<ArrowUp color="royalblue" size={18} />)}
        </th>
        <th width="23%">{t('value')}</th>
        <th width="25%">{t('labels')}</th>
        <th width="25%">{t('location')}</th>
      </tr>
    </thead>

    <tbody>
      {videoSegmentList.map((segment, index) => {
        return <SegmentItem key={segment._id} segment={segment} index={index + pageNumber * defaultPageSize + 1} />
      })}
    </tbody>
  </Table>
);

VideoSegmentTable.propTypes = {
  videoSegmentList: PropTypes.instanceOf(Array),
  t: PropTypes.func.isRequired,
  onSort: PropTypes.func.isRequired,
  pageNumber: PropTypes.number,
  sortData: PropTypes.shape({
    sortField: PropTypes.oneOf([ PropTypes.string, PropTypes.object ]),
    sortDirection: PropTypes.number,
  }).isRequired,
};

VideoSegmentTable.defaultProps = {
  videoSegmentList: [],
  pageNumber: 0,
};

export default withTranslation()(VideoSegmentTable);