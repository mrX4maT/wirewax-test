import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';

import mainEN from './locales/en/main';

i18n.use(initReactI18next).init({
  resources: {
    en: {
      translation: mainEN,
    },
  },
  lng: 'en',
  fallbackLng: 'en',
  keySeparator: false,
  react: {
    wait: true,
  },
});
