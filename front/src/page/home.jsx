import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Pagination, Form, Alert } from 'react-bootstrap';
import { getVideoSegmentData } from '../store/action/videoSegment';
import config from 'react-global-configuration';
import _ from 'lodash';

import VideoSegmentTable from '../component/videoSegmentTable';

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentPage: 0,
      sortField: null,
      sortDirection: 1, // where 1 is ascending and -1 is descending
      pageSize: config.get('videoSegmentQuerySettings.pageSize'),
      locationFilter: [
        {
          active: false,
          label: 'Centre',
          name: 'centre'
        },
        {
          active: false,
          label: 'Left',
          name: 'left'
        },
        {
          active: false,
          label: 'Right',
          name: 'right'
        },
        {
          active: false,
          label: 'Upper',
          name: 'upper'
        },
        {
          active: false,
          label: 'Lower',
          name: 'lower'
        },
      ]
    }
  }

  componentDidMount() {
    this.fetchVideoSegmentData();
  }

  fetchVideoSegmentData = () => {
    const { getVideoSegmentData } = this.props;
    const { pageSize, sortField, sortDirection, locationFilter } = this.state;

    const filter = locationFilter
      .filter(location => location.active)
      .map(location => location.name)
      .join(' ');

    getVideoSegmentData({ skip: this.pageOffset(), limit: pageSize, filter, sortField, sortDirection });
  }

  pageOffset = () => {
    const {currentPage, pageSize} = this.state;

    return currentPage * pageSize
  }

  pushTo = (page) => {
    this.setState({ currentPage: page }, () => {
      this.fetchVideoSegmentData();
    });
  };

  filterDataByLocation = type => {
    const { locationFilter } = this.state;

    const index = _.findIndex(locationFilter, { name: type.name });

    if (~index) {
      locationFilter[index].active = !locationFilter[index].active;
      
      this.setState({
        locationFilter: [...locationFilter],
      }, () => {
        this.fetchVideoSegmentData();
      });
    }
  }

  onSort = (fieldName) => {
    const { sortField, sortDirection } = this.state;

    if (sortField !== fieldName) {
      this.setState({
        sortField: fieldName,
        sortDirection: 1,
      }, () => {
        this.fetchVideoSegmentData();
      });

      return;
    }

    if (sortDirection === -1) {
      this.setState({
        sortField: null,
        sortDirection: 1,
      }, () => {
        this.fetchVideoSegmentData();
      });

      return;
    }

    this.setState({
      sortDirection: -1,
    }, () => {
      this.fetchVideoSegmentData();
    });
  }

  renderFilterGroup = () => {
    const { locationFilter } = this.state;

    return <Alert className='location-filter' variant={'light'}>
      {locationFilter.map(type => (
        <Form.Group controlId={type.name} key={type.name}>
          <Form.Check
            type="checkbox"
            key={type.name}
            label={type.label}
            checked={type.active}
            onChange={() => this.filterDataByLocation(type)}
          />
        </Form.Group>
      ))}
    </Alert>
  }

  render() {
    const { loaded, videoSegmentList } = this.props;
    const { pageSize, currentPage, sortField, sortDirection } = this.state;

    if (!loaded) {
      return null;
    }

    const pageNumber = currentPage ? currentPage : 0;
    const pageCount = Math.ceil(videoSegmentList.totalRecords / pageSize);

    return (
      <div className="page-content content">
        <h1>WIREWAX TEST</h1>
        <hr />

        { pageCount > 1 && (
          <div className="pagination-panel">
            <Pagination size="sm">
              <Pagination.First disabled={pageNumber === 0} onClick={() => this.pushTo(0)} />
              <Pagination.Prev disabled={pageNumber === 0} onClick={() => this.pushTo(pageNumber - 1)} />
              <Pagination.Item active={pageNumber === 0} onClick={() => this.pushTo(0)}>{1}</Pagination.Item>
              { pageNumber > 3 && (<Pagination.Ellipsis disabled />)}

              { pageNumber > 2 && (<Pagination.Item onClick={() => this.pushTo(pageNumber - 2)}>{pageNumber - 1}</Pagination.Item>)}
              { pageNumber > 1 && (<Pagination.Item onClick={() => this.pushTo(pageNumber - 1)}>{pageNumber}</Pagination.Item>)}
              { pageNumber > 0 && pageNumber < (pageCount - 1) && (<Pagination.Item active>{pageNumber + 1}</Pagination.Item>)}
              { pageNumber < (pageCount - 2) && (<Pagination.Item onClick={() => this.pushTo(pageNumber + 1)}>{pageNumber + 2}</Pagination.Item>)}
              { pageNumber < (pageCount - 3) && (<Pagination.Item onClick={() => this.pushTo(pageNumber + 2)}>{pageNumber + 3}</Pagination.Item>)}

              { pageNumber < (pageCount - 4) && (<Pagination.Ellipsis disabled />)}
              <Pagination.Item active={pageNumber === (pageCount - 1)} onClick={() => this.pushTo(pageCount - 1)}>{pageCount}</Pagination.Item>
              <Pagination.Next disabled={pageNumber === (pageCount - 1)} onClick={() => this.pushTo(pageNumber + 1)} />
              <Pagination.Last disabled={pageNumber === (pageCount - 1)} onClick={() => this.pushTo(pageCount - 1)} />
            </Pagination>
            
          </div>
        )}
        
        {this.renderFilterGroup()}
        <VideoSegmentTable videoSegmentList= {videoSegmentList.data} onSort={this.onSort} sortData={{sortField, sortDirection }} />
      </div>
    );
  }
}

Home.propTypes = {
  getVideoSegmentData: PropTypes.func.isRequired,
  loaded: PropTypes.bool,
  videoSegmentList: PropTypes.shape()
};

Home.defaultProps = {
  loaded: false,
  videoSegmentList: null
};

const dispatchStateToProps = (dispatch) => ({
  getVideoSegmentData: (data) => dispatch(getVideoSegmentData(data)),
});

const mapStateToProps = (store) => ({
  loaded: store.videoSegment.loaded,
  videoSegmentList: store.videoSegment.videoSegmentList,
});

export default connect(mapStateToProps, dispatchStateToProps)(Home);