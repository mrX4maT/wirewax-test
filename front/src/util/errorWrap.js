import _ from 'lodash';

export default (error) => {
  if (_.isString(error)) {
    return [{ message: error }];
  }

  if (_.isObject(error) && !_.isArray(error)) {
    return [{
      ...error,
      message: error.message || error.data,
    }];
  }

  return error;
};