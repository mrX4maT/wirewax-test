import { all } from 'redux-saga/effects';
import videoSegment from './videoSegment';

export default function* rootSaga() {
  yield all([
    ...videoSegment,
  ]);
}