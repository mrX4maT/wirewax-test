import { takeLeading, call, put } from 'redux-saga/effects';
import config from 'react-global-configuration';
import axios from 'axios';
import { v4 as uuid } from 'uuid';
import queryString from 'query-string';

import errorWrap from '../../util/errorWrap';

import { REQUEST_SEGMENT_DATA } from '../constant/videoSegment';
import { requestVideoSegmentDataSuccess, requestVideoSegmentDataError } from '../action/videoSegment';

import { hideLoader, showLoader } from '../action/loader';

const baseUrl = `${config.get('backend.url')}${config.get('backend.apiPrefix')}`;

function* getData({
    skip,
    limit,
    sortField,
    sortDirection,
    filter,
}) {
  const operationId = uuid();
  yield put(showLoader(operationId));

  try {
    const res = yield call(axios, {
      method: 'get',
      url: `${baseUrl}/videoData?${queryString.stringify({
      skip,
      limit,
      sortField,
      sortDirection,
      filter,
      })}`,
    });

    if (res.data) {
      yield put(requestVideoSegmentDataSuccess(res.data));
    }
  } catch (error) {
    if (error && error.response && error.response.data) {
      yield put(requestVideoSegmentDataError(errorWrap(error.response.data)));
      console.error(error.response.data);
    }
  }
  
  yield put(hideLoader(operationId));
}

export default [
  takeLeading(REQUEST_SEGMENT_DATA, getData),
];