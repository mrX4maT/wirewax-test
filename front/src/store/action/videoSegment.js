import {
  REQUEST_SEGMENT_DATA,
  REQUEST_SEGMENT_DATA_SUCCESS,
  REQUEST_SEGMENT_DATA_ERROR,
} from '../constant/videoSegment';

export const getVideoSegmentData = ({
  skip,
  limit,
  sortField,
  sortDirection,
  filter,
}) => ({
  type: REQUEST_SEGMENT_DATA,
  skip,
  limit,
  sortField,
  sortDirection,
  filter,
});

export const requestVideoSegmentDataSuccess = data => ({
  type: REQUEST_SEGMENT_DATA_SUCCESS,
  data,
});

export const requestVideoSegmentDataError = error => ({
  type: REQUEST_SEGMENT_DATA_ERROR,
  error
});
