import { 
  REQUEST_SEGMENT_DATA_SUCCESS,
  REQUEST_SEGMENT_DATA_ERROR
} from '../constant/videoSegment';

const initial = {
  videoSegmentList: null,
  totalRecords: null,
  error: null,
  loaded: false,
};

const videoSegmentReducer = (state = initial, action) => {
  switch (action.type) {
    case REQUEST_SEGMENT_DATA_SUCCESS: {
      return {
        ...state,
        total: action.data.totalRecords,
        videoSegmentList: action.data,
        loaded: true,
      };
    }
    
    case REQUEST_SEGMENT_DATA_ERROR: {
      return {
        ...state,
        error: action.error,
      };
    }

    default:
      return state;
  }
};

export default videoSegmentReducer;