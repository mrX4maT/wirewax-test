import { combineReducers } from 'redux';

import loader from './loader';
import videoSegment from './videoSegment';

export default combineReducers({
  loader,
  videoSegment,
});