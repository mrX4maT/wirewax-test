import config from 'config';
import express from 'express'
import bodyParser from 'body-parser';
import cors from 'cors';

import db from './component/db/index';
import bootstrap from './component/bootstrap';

/**
 * @file server entrypoint with
 * @author Maksym S.
 * init database
 * load app settings
 * load app routes
 * start the server 
 */

const main = async () => {
  try {
    await db.init();

    const app = express();

    app.use(cors());
    app.use(bodyParser.json(config.bodyParser.json));
    app.use(bodyParser.urlencoded(config.bodyParser.urlencoded));

    bootstrap.routes(app);

    app.listen(process.env.PORT || config.http.port,
      () => console.log(`${new Date()}: Server started on ${process.env.PORT || config.http.port} port`));
  } catch (err) {
    console.error(err);
  }
};

main();
