import _ from 'lodash';
import config from 'config';

import db from '../component/db';

const model = db.models.videoSegment;

/**
 * @file VideoSegmentModel
 * @author Maksym S.
 * @function getFilteredRecordsCount - return documents count based on query params {filter, skip, limit, sortField, sortDirection}
 * @function getAll - return documents collection based on query query params {filter, skip, limit, sortField, sortDirection}
 */

class VideoSegmentModel {
  make({
    in_frame = null,
    out_frame = null,
    duration = null,
    content = {},
    data_type_id = '',
    tech_id = '',
  } = {}) {
    return model.create({
      in_frame,
      out_frame,
      duration,
      content,
      data_type_id,
      tech_id,
    });
  }

  getFilteredRecordsCount({
    filter = '',
  }) {
    const query = {};

    if (filter) {
      query.$text = {
        $search: filter, 
        $caseSensitive: false
      }
    }

    return model.countDocuments(query)
    .lean()
    .exec();
  }

  getAll({
    limit = config.videoSegmentQuerySettings.limit,
    skip = config.videoSegmentQuerySettings.skip,
    sortField = null,
    sortDirection = config.videoSegmentQuerySettings.sortDirection,
    filter = '',
  } = { ...config.videoSegmentQuerySettings }) {

    const query = {};

    if (filter) {
      query.$text = {
        $search: filter, 
        $caseSensitive: false,
      }
    }

    const req = model.find(query)

    if(sortField && sortDirection) {
      req.sort({
        [sortField]: +sortDirection,
      })
    }

    return req.limit(+limit)
    .skip(+skip)
    .lean()
    .exec();
  }
}

export default new VideoSegmentModel();