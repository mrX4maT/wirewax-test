import segmentModel from '../model/videoSegment';

/**
 * @file action, get all Video Segment data
 * include validation
 * @author Maksym S.
 */

class VideoActions {
  async getVideoData(query) {
    return {
      totalRecords: await segmentModel.getFilteredRecordsCount(query),
      data: await segmentModel.getAll(query)
    };
  }
}

export default new VideoActions();
