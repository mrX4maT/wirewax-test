import mongoose from 'mongoose';

const segmentContentSchema = new mongoose.Schema({
  value: [String],
  labels: [String],
  graphics_type_value: [String],
  location: {
    type: [String],
    index: true,
  },
  sample_frame: { 
    type: Number,
    required: true,
  },
}, { _id: false });

segmentContentSchema.index({ location: 'text' });

export default segmentContentSchema;
