import {
  Schema
} from 'mongoose';
import _ from 'lodash';

/**
 * @file mongo db schema based on provided JSON data
 * @author Maksym S.
 */

import contentSchema from './_segmentContent';

const segment = new Schema({
  in_frame: {
    type: Number,
    required: true,
    index: true
  },
  out_frame: {
    type: Number,
    required: true,
    index: true
  },
  duration: {
    type: Number,
    required: true,
  },
  content: {
    type: contentSchema,
    required: true
  },
  data_type_id: {
    type: String,
    required: true,
  },
  tech_id: {
    type: String,
    required: true,
  },
  _id: false
});

segment.index({
  in_frame: 1,
  out_frame: 1,
}, {
  unique: false,
});

export default segment;