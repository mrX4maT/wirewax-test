import { Router } from 'express';
import config from 'config';

import videoActions from '../action/video';
import videoValidator from '../validator/videos';
import basic from '../component/middleware/wrapper';

/**
 * @file router for the '/videoData' route, bound to swagger-ui-express
 * @author Maksym S.
 * gets all video segments, with custim query params
 * @var limit pageSize param
 * @var skip Skips the first number documents
 * @var filter Filter video segment by partial match search in content location
 * @var sortDirection where value 1 - sort by asc, and -1 sort by desc
 * @var sortField in_frame / out_frame sortable field name
 */

const router = Router();
const apiPrefix = config.application.apiPrefix;

router.get(`${apiPrefix}/videoData`, basic.wrap(async (req)=>{
  const { query } = req;

  await videoValidator.paginate(query);

  return videoActions.getVideoData(query);
}));

export default router;
