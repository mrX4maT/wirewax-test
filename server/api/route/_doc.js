import { Router } from 'express';

const router = Router();

const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('../swagger.json');

/**
 * @file router for the '/api-docs' route, bound to swagger-ui-express
 * @author Maksym S.
 */

router.use('/api-docs', swaggerUi.serve);
router.get('/api-docs', swaggerUi.setup(swaggerDocument));


export default router;
