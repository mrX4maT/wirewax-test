import express, { Router } from 'express';
import path from 'path';
import fs from 'fs';

const router = Router();

const frontPath = path.join(__dirname, '../../../front/build');

/**
 * @file static route
 * @author Maksym S.
 * combine client build with app backend
 * 404 redirect to client, error page
 */

router.get('/*', (req, res, next) => {
  if (req.url === '/' || fs.existsSync(path.join(frontPath, req.url))) {
    express.static(frontPath)(req, res, next);
    return;
  }

  res.sendFile(path.join(frontPath, 'index.html'));
});

export default router;