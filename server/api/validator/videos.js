import _ from 'lodash';

import validator from '../component/validator';

/**
 * @file validator for the '/videoData'
 * @author Maksym S.
 * simple validator, to check skip query param
 * @var skip requered and must be a number
 */

class VideoValidate {
  paginate(body = {}) {
    const data = {
      skip: Number(body.skip),
    };

    const errorList = validator.validate(data, {
      skip: {
        type: 'number',
        empty: false,
      },
    });

    if (_.isArray(errorList)) {
      throw (errorList);
    }

    return _.pick(data, ['skip']);
  }
}

export default new VideoValidate();
