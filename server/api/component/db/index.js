import mongoose from 'mongoose';
import config from 'config';
import fs from 'fs';
import path from 'path';

/**
 * @file simple mongo database init file, based on env settings
 * link all db models from db models folder
 * @author Maksym S.
 */

const _parse = (initPath, callback) => {
  fs.readdirSync(initPath).forEach((name) => {
    const itemPath = path.join(initPath, name);
    const stat = fs.statSync(itemPath);

    if (stat && stat.isDirectory(itemPath)) {
      _parse(itemPath, callback);
    } else {
      callback(itemPath);
    }
  });
};

class Mongo {
  constructor() {
    this.models = {};
  }

  async init() {
    const db = mongoose.connection;

    db.on('error', (error) => {
      console.error('error', error);
    });

    await mongoose.connect(config.mongo.url, {
      useUnifiedTopology: true,
      useNewUrlParser: true,
      useCreateIndex: true,
    });

    console.log(`Connected to: '${config.mongo.url}'`);
    this.createModels();
  }

  createModels() {
    _parse(path.join(__dirname, '../..', 'db'), (itemPath) => {
      try {
        this.models[path.basename(itemPath).split('.')[0]] = mongoose.model(path.basename(itemPath).split('.')[0], require(itemPath).default);
      } catch (e) {
        console.error(e);
      }
    });
  }
}

export default new Mongo();