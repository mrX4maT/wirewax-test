import path from 'path';
import fs from 'fs';
import _ from 'lodash';

/**
 * @file bootstrap router helper
 * combine all server routes together
 * @author Maksym S.
 */

const _parse = (initPath, callback) => {
  fs.readdirSync(initPath).forEach((name) => {
    const itemPath = path.join(initPath, name);
    const stat = fs.statSync(itemPath);

    if (stat && stat.isDirectory(itemPath)) {
      _parse(itemPath, callback);
    } else {
      callback(itemPath);
    }
  });
};

class Bootstrap {
  routes(app) {
    if (fs.existsSync(path.join(__dirname, '..', 'route'))) {
      _parse(path.join(__dirname, '..', 'route'), (itemPath) => {
        const router = require(itemPath);

        _.keys(router).forEach((i) => {
          if (router[i]) {
            app.use(router[i]);
          }
        });
      });
    }
  }
}

export default new Bootstrap();