import FastestValidator from 'fastest-validator';

class Validator {
  constructor() {
    const validator = new FastestValidator({
      messages: {
        invalidField: "Invalid field '{field}'",
      },
    });

    // Register a custom 'mongoId' validator
    validator.add('mongoId', (_id, options) => {
      if (options.optional && !_id) {
        return true;
      }

      const mongoose = require('mongoose');
      if (mongoose.Types.ObjectId.isValid(_id)) {
        return true;
      }

      return validator.makeError('invalidField', 'mongoose.Types.ObjectId', _id);
    });

    return validator;
  }
}

export default new Validator();