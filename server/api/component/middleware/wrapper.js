import _ from 'lodash';
import config from 'config';

export class Middleware {
  static error(error) {
    if (error instanceof Error) {
      const message = config.staging ? error.stack || error.message : error.message || 'There was an error processing your request. Please try again later.';
      return { message };
    }

    if (_.isString(error)) {
      return { message: error };
    }

    if (_.isObject(error)) {
      return error;
    }

    return { message: 'Unknown server error' };
  }

  wrap(middleware) {
    return async (req, res, next) => {
      try {
        const result = await middleware(req, res, next);

        if (_.isObject(result)) {
          res.json(result);
          return;
        }

        res.send(result);
      } catch (err) {
        console.error(err)
        res.status(err.status || 400).json(Middleware.error(err));
      }
    };
  }
}

export default new Middleware();
