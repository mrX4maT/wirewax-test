# WIREWAX TEST
client, server app
### Tech
* [node.js] - evented I/O for the backend
* [Express] - fast node.js network app framework [@tjholowaychuk]
* [MongoDB] - nosql database 
* [ReactJs] - React js library

# Getting Started!
 Fill database from json mock data:

```sh
$ mongoimport --uri "mongodb://localhost:27017/wirewax-db-dev" --collection videosegments --file ./mockData/videoSegment.json --batchSize 1 --jsonArray
```
### Installation

requires [Node.js](https://nodejs.org/) >=12.16.2 to run.

Install the dependencies and devDependencies and start the server.

by default environment settings store in:

- server/config/default.json
- front/src/config/default.json

For production environments...
if you need change production configuration, you need to add variables and settings to:
- server/config/production.json
- front/src/config/production.json
```sh
$ yarn
$ yarn start
```
to website access use:
```sh
http://localhost:4000
```

### Development

Server run
```sh
$ cd server
$ yarn
& yarn start
```

Client run
```sh
$ cd front
$ yarn
& yarn start
```

to website access use:
```sh
http://localhost:3000
```


#### Building for source
For production release:
```sh
$ yarn
$ yarn build
```

# API SERVER DOC
```sh
$ yarn
$ yarn start

http://localhost:4000/api-docs/
```